"""Extractor for email attachments

Local usage: python3 src/main.py
"""
import os
import csv
import json
import re
from logging import getLogger, basicConfig, DEBUG, INFO
import imaplib
import email
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from openpyxl import load_workbook
from io import BytesIO, TextIOWrapper, StringIO
from datetime import datetime
from glob import glob
from base64 import b64decode
import codecs
import smtplib, ssl


basicConfig(
format="[{asctime}] [{levelname}] [line: {lineno}]: {message}",
style="{",
level=INFO
)

logger = getLogger(__name__)

config_path = "/config/config.json"
output_directory = "/data/out/tables"

if not os.path.exists(config_path):
    raise Exception("Configuration file not found")
with open(config_path) as conf_file:
    conf = json.load(conf_file)

if not os.path.exists(output_directory):
    os.makedirs(output_directory)

def send_email_notification():
    """Send email notification.
    """
    message = MIMEMultipart("alternative")
    message["Subject"] = conf["email_subject"]
    sender = conf["user"]
    message["From"] = sender
    part = MIMEText(conf["email_body"], "plain")
    message.attach(part)
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(conf["smtp_server"], 465, context=context) as server:
        server.login(sender, conf["password"])
        for receiver in conf["slack_emails"]:
            message["To"] = receiver
            server.sendmail(sender, receiver, message.as_string())

def connect():
    """Connect to target email account Inbox.

    Returns:
        c {imaplib.IMAP4_SSL} -- connection to email Inbox folder
    """
    c = imaplib.IMAP4_SSL(conf["server"])
    c.login(conf["user"], conf["password"])
    c.select("Inbox")
    return c

def get_files_list():
    """Get current list of output files.

    Returns:
        files_list {list} -- current list of output files
    """
    output_files = glob("{}/*.csv".format(output_directory))
    files_list = []
    for f in output_files:
        basename = os.path.basename(f)
        if re.match(conf["file_name"], basename):
            files_list.append(basename)
    return files_list

def remove_bom(path):
    """Remove binary characters.
    
    Arguments:
        path {str} -- path to file
    """
    BUFSIZE = 4096
    BOMLEN = len(codecs.BOM_UTF8)

    #binary downloaded files
    with open(path, "r+b") as fp:
        chunk = fp.read(BUFSIZE)
        if chunk.startswith(codecs.BOM_UTF8):
            i = 0
            chunk = chunk[BOMLEN:]
            while chunk:
                fp.seek(i)
                fp.write(chunk)
                i += len(chunk)
                fp.seek(BOMLEN, os.SEEK_CUR)
                chunk = fp.read(BUFSIZE)
            fp.seek(-BOMLEN, os.SEEK_CUR)
            fp.truncate()

    # string downloaded files
    with open(path, "r", encoding="utf-8") as fin:
        if fin.read(3) == 'ï»¿':
            lines = fin.readlines()
            lines[0] = lines[0][0:]
            bom = True
        else:
            bom = False
    if bom:
        with open(path, "w", encoding="utf-8") as fout:
            fout.writelines(lines)

## Convert CSVs not conforming to UNIX standard
def convert_csv(path, destination):
    """Convert ccv to unix format

    Arguments:
        paht {srt} -- path to file to be converted

        destination {str} -- destination for converted file
    """
    remove_bom(path)
    sniffer = csv.Sniffer()
    with open(path, encoding="utf-8") as infile:
        dialect = sniffer.sniff(infile.readline())
        ## file is invalid or we are to append filename column
        if dialect.delimiter != csv.unix_dialect.delimiter or dialect.escapechar != csv.unix_dialect.escapechar or dialect.lineterminator != csv.unix_dialect.lineterminator or dialect.quoting != csv.unix_dialect.quoting or include_filename:
            infile.seek(0)
            reader = csv.reader(infile, dialect=dialect)
            with open(destination, "w", encoding="utf-8", newline='') as outfile:
                writer = csv.writer(outfile, dialect=csv.unix_dialect)
                writer.writerows(reader)
            return
    ## file is valid
    os.rename(path, destination)

def union():
    """Union output tables.
    """ 
    union_list = get_files_list()
    if not union_list:
        logger.warning("No new files to union!")
        send_email_notification()
    else:
        final_directory = "{}/{}.csv".format(output_directory, conf["output_name"])
        logger.info("Union for output tables: %s", ",".join(union_list))
        union_file = "/tmp/union.csv"
        with open(union_file,"w", encoding='utf-8', newline='') as file_merged:
            for (i, name) in enumerate(union_list):
                file_directory = "{}/{}".format(output_directory, name)
                with open(file_directory, "r", encoding='utf-8') as file_part:
                    if i != 0:
                        next(file_part)
                    file_merged.writelines((f"{line.strip()}\n" for line in file_part if line.strip()))
                    
        convert_csv(union_file, final_directory)
        # delete original files from directory
        for unioned_file in union_list:
            file_directory = "{}/{}".format(output_directory, unioned_file)
            logger.info("Deleting original unioned file: %s", unioned_file)
            if os.path.isfile(file_directory):
                os.remove(file_directory)

def excel_to_csv(file_directory):
    """Convert excel to csv.

    Arguments:
        file_directory {str} -- path to excel file to be converted
    """
    workbook = load_workbook(file_directory)
    sheet_name = workbook.sheetnames[0]
    basename = os.path.basename(file_directory)
    name = basename.split(".")[0]
    sheet = workbook[sheet_name]
    if os.path.isfile("{}/{}.csv".format(output_directory, name)):
        os.remove("{}/{}.csv".format(output_directory, name))
    with open("{}/{}.csv".format(output_directory, name), "w", encoding="utf-8", newline="") as fid:
        w = csv.writer(fid, dialect=csv.unix_dialect)
        for row in sheet.rows:
            w.writerow([c.value for c in row])
    # delete original excel files from directory
    logger.info("Deleting original excel file: %s", basename)
    if os.path.isfile(file_directory):
        os.remove(file_directory)

def rename(file_name):
    """Rename file

    Arguments:
        file_name {str} -- name of file to be renamed

    Returns:
        file_directory {str} -- path to renamed file
    """
    current_timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")
    name = file_name.split(".")[0]
    file_directory = "{}/{}_{}.csv".format(output_directory, name, current_timestamp)
    return file_directory

def donwload_convert(connection, part, email_id):
    """Donwload attachments of particular email.

    Arguments:
        connection {imaplib.IMAP4_SSL} -- connection to email Inbox folder

        part {email.message.Message} -- part of email message

        email_id {str} -- email id
    """
    file_name = translate_filename(part.get_filename())
    current_files = get_files_list()
    logger.info("Saving email attachment '%s'", file_name)
    if file_name in current_files:
        file_directory = rename(file_name)
    else:
        file_directory = "{}/{}".format(output_directory, file_name)
    with open(file_directory, "wb") as fid:
        fid.write(part.get_payload(decode=True))
    # flag mail as seen
    logger.info("Flag email as seen")
    resp, data = connection.store(email_id, '+FLAGS', '\\Seen')
    # convert excel to csv if it's needed
    if file_directory.endswith(".csv"):
        logger.info("Attached file is csv, comversion is not necessary")
    else:
        logger.info("Converting excel to csv...")
        excel_to_csv(file_directory)

def translate_filename(fname):
    """Translate name if it's encoded

    Arguments:
        fname {str/bytes} -- filename in string or byte format

    Returns:
        {str} -- filename in string format
    """
    if not "B?" in fname and not fname.endswith("?="):
        return fname
    return b64decode(fname[fname.find("B?")+2:]).decode("utf-8")

def download_attachments(connection, email_id):
    """Donwload attachments of particular email.

    Arguments:
        connection {imaplib.IMAP4_SSL} -- connection to email Inbox folder

        email_id {str} -- email id
    """
    resp, data = connection.fetch(email_id, "(BODY.PEEK[])")
    email_body = data[0][1]
    mail = email.message_from_bytes(email_body)
    sender = mail.get("From")
    if mail.get_content_maintype() != 'multipart' or sender in conf["blacklist"]:
        return
    no_attachment = True
    for part in mail.walk():
        if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
            no_attachment = False
            attachment_name = translate_filename(part.get_filename())
            if "file_name" in conf.keys() and conf["file_name"]:
                if re.match(conf["file_name"], attachment_name):
                    donwload_convert(connection, part, email_id)
                else:
                    continue
            else:
                donwload_convert(connection, part, email_id)
    if no_attachment:
        logger.warning("No attachments!")
        # flag mail as seen
        #logger.info("Flag as seen")
        #resp, data = connection.store(email_id, '+FLAGS', '\\Seen')

def main():
    """Main function for extraction of email attachments and saving them to csv.
    """
    logger.info("Connecting to email account...")
    c = connect()
    #typ, msgs = c.search(None,'(UNSEEN SUBJECT "%s")' % conf["subject"])
    typ, msgs = c.search(None,'(UNSEEN)')
    msgs = msgs[0].split()
    if msgs:
        for email_id in msgs:
            download_attachments(c, email_id)
        # union tables
        if "union" in conf and conf["union"]:
            union()
    else:
        logger.warning("No new unseen files!")
        send_email_notification()


if __name__ == "__main__":
    main()
