# ex-attachments
This extractor extracts all attachments from email.

## config.json

```json
{
    "server": "imap.gmail.com",
    "user": "pokuspokusovy918",
    "password": "",
    "subject": "pokus",
    "file_name": "MR90133 crocodille data(.*)",
    "union": true,
    "output_name": "MR90133_crocodille_data",
    "smtp_server": "smtp.gmail.com",
    "slack_emails": [],
    "email_subject": "Warning from ex-attachments",
    "email_body": "No new files from Ahold!",
    "blacklist": "Google <no-reply@accounts.google.com>"
}
```

## Local use

```sh
python3 src/main.py
```