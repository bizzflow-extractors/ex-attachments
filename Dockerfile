FROM python:3.7-alpine

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="Bizzflow"
LABEL com.bizztreat.component="ex-attachments"
LABEL com.bizztreat.title="Email Attachments Extractor"

RUN python -m pip install --upgrade pip setuptools

RUN python -m pip install --no-cache-dir --upgrade openpyxl


VOLUME /data/out/tables
VOLUME /config

ADD src/ /code

WORKDIR /code

# Edit this any way you like it
ENTRYPOINT ["python", "-u", "main.py"]
